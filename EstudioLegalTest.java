package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;



import org.junit.Before;

public class EstudioLegalTest {
    Cliente cliente1;
    Cliente cliente2;
    Cliente cliente3;
    @Before
    public void initObject(){
        DomicilioSimple domicilio1= new DomicilioSimple("Mariano Moreno", 43);
        cliente1= new Cliente("Jose","Robles",4323762,domicilio1,"JoseRobles2020@gmail.com",38349283);
        DomicilioEdificio domicilio2= new DomicilioEdificio("Avenida Belgrano",543,2,25);
        cliente2= new Cliente("Juan","Bazan",4157721,domicilio2,"JuanBazan2020@gmail.com",38342343);
        DomicilioBarrial domicilio3= new DomicilioBarrial("Barrio Malvinas Argentinas",54,"2");
        cliente3= new Cliente("Mariana","Gonzales",4197321,domicilio3,"MariaGonzales2020@gmail.com",3836287);
    }

    @Test
    public void crearEstudioLegalyAgregarClientes(){
        EstudioLegal estudio1 =new EstudioLegal("Ramirez y Asociados");
        estudio1.agregarClientes(cliente1);
        estudio1.agregarClientes(cliente2);
        estudio1.agregarClientes(cliente3);
        assertEquals(cliente1,estudio1.buscarClientexDni(4323762));
    }    
    
}
