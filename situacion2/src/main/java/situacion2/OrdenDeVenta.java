package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdenDeVenta {
    private Empresa empresa;
    private ArrayList<Producto> productos;
    private BigDecimal precioTotal;

    public OrdenDeVenta() {
        productos=new ArrayList<Producto>();
    }



    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public BigDecimal getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(){
        BigDecimal aux2=new BigDecimal("0");
        for(Producto aux: productos){
            aux2= aux2.add(aux.getPrecio());
        }
        precioTotal=aux2;
    }


    public void agregarProductos(Producto producto){
        productos.add(producto);
        setPrecioTotal();
    }


    public void mostrarProductos(){
        for(Producto var:productos){
            var.mostrarProducto();
        }
    }

    


}
