package situacion2;

import java.math.BigDecimal;

public class HerramientaManual extends Herramienta {

    public HerramientaManual(Integer codigo, String nombre, BigDecimal precio, String funcionalidad) {
        super(codigo, nombre, precio, funcionalidad);
    }

    public void mostrarProducto() {
        System.out.println("Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\nFuncionalidad: " + getFuncionalidad()+ "\n-------------------------------------------------------");

    }

   
    
}
