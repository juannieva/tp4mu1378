package situacion2;

import java.math.BigDecimal;

public class HerramientaElectrica extends Herramienta {
    private BigDecimal consumoElectrico;

    public HerramientaElectrica(Integer codigo, String nombre, BigDecimal precio, String funcionalidad,
            BigDecimal consumoElectrico) {
        super(codigo, nombre, precio, funcionalidad);
        this.consumoElectrico = consumoElectrico;
    }
    

    public BigDecimal getConsumoElectrico() {
        return consumoElectrico;
    }

    public void setConsumoElectrico(BigDecimal consumoElectrico) {
        this.consumoElectrico = consumoElectrico;
    }

    public void mostrarProducto(){
        System.out.println("Codigo: "+getCodigo()+"\nNombre: "+getNombre()+"\nPrecio: "+getPrecio()+"\nFuncionalidad: "+getFuncionalidad()+"\nConsumo: "+getConsumoElectrico()+"\n-------------------------------------------------------");
    }
    
    
    
}
