package situacion2;

import java.math.BigDecimal;

public abstract class Herramienta extends Producto {
    private String funcionalidad;

    public Herramienta(Integer codigo, String nombre, BigDecimal precio, String funcionalidad) {
        super(codigo, nombre, precio);
        this.funcionalidad = funcionalidad;
    }
    
    public String getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(String funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    public abstract void mostrarProducto();
    

  
    
}
