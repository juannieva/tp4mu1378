package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdenDeCompra {
    private Proveedor proveedor;
    private ArrayList<MateriaPrima> materiasPrimas;
    private BigDecimal precioTotal;

    public OrdenDeCompra(){
        materiasPrimas=new ArrayList<MateriaPrima>();
    }


    public void mostrarMateriasPrimas(){
        for(MateriaPrima var: materiasPrimas){
            var.mostrarProducto();
            System.out.println("-------------------------------------------------------");
        }
    }
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public BigDecimal getPrecioTotal() {
        return precioTotal;
    }

    public void agregarMateriaPrima(MateriaPrima materiaprima){
        materiasPrimas.add(materiaprima);
        setPrecioTotal();
    }

    public void setPrecioTotal() {
        precioTotal=new BigDecimal("0");
        for(MateriaPrima var: materiasPrimas){
            precioTotal=precioTotal.add(var.getPrecio());
        }
    }
    
}
