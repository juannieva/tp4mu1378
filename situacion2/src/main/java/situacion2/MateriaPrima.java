package situacion2;

import java.math.BigDecimal;

public class MateriaPrima extends Producto {
    private Proveedor proveedor;

    
    public MateriaPrima(Integer codigo, String nombre, BigDecimal precio) {
        super(codigo, nombre, precio);
    }
    

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public void mostrarProducto() {
        System.out.println("Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\n-------------------------------------------------------");

    }
    
}
