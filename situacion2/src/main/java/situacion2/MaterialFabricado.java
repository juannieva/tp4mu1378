package situacion2;

import java.math.BigDecimal;

public class MaterialFabricado extends Producto {

    public MaterialFabricado(Integer codigo, String nombre, BigDecimal precio) {
        super(codigo, nombre, precio);
    }

    public void mostrarProducto() {
        System.out.println("Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\n-------------------------------------------------------");

    }
    
    
}
