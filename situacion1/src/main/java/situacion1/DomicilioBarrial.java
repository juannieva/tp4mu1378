package situacion1;

public class DomicilioBarrial extends Domicilio {
    private String manzana;

    public DomicilioBarrial(String calleoNombreBarrio, Integer numero, String manzana) {
        super(calleoNombreBarrio, numero);
        this.manzana=manzana;
    }

    public String mostrarDomicilio(){
        return ("Barrio "+super.getCalleoNombreBarrio()+",Manzana "+manzana+"\n,Numero:"+super.getNumero());
    }
    

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    
}
