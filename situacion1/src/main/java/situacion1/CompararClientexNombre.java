package situacion1;

import java.util.Comparator;

public class CompararClientexNombre implements Comparator<Cliente> {
    @Override
    public int compare(Cliente c1, Cliente c2) {
        
        return (c1.getApellido().compareToIgnoreCase(c2.getApellido()));
    }
}
