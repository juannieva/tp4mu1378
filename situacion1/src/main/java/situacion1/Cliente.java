package situacion1;

public class Cliente implements Comparable<Cliente> {
    private String nombre;
    private String apellido;
    private Integer dni;
    private Domicilio domicilio;
    private String correoElectronico;
    private Integer telefono;

    public Cliente(String nombre, String apellido, Integer dni, Domicilio domicilio, String correoElectronico,
    Integer telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.domicilio = domicilio;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
    }

    public String mostrarCliente(){
        return ("Nombre y Apellido: "+nombre+" "+apellido+"\nDNI: "+dni+"\nDomicilio:\n"+domicilio.mostrarDomicilio()+
        "\nCorreo Electronico: "+correoElectronico+"\nTelefono: "+telefono+"\n\n");
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    
    @Override
    public int compareTo(Cliente c){
        int resultado=0;
        if (this.dni<c.dni) {  resultado = -1;    }
            else if (this.dni>c.dni) {   resultado = 1;   }
        else {   resultado = 0;   }
        return resultado;
        /*if(c.getDni()>dni){
            return -1;
        }
        else{
            if(c.getDni()>dni){
                return 0;
            }
            else{
                return 1;
            }
        }*/

    }
    
}
